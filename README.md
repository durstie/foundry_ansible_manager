# Introduction
This is a simple ansible environment to manage multiple Foundry VTT instances on the same server.

## Foundry Licensing
**Very Important**: It is not against Foundry VTT ToS to run multiple instances of Foundry VTT on the same server at the same time.  However, it is against ToS if you use the same license/key on more than one Foundry VTT instance running at the same time.

**TLDR**: Each running instance must have its own license/key.

## Prerequisites
### Purpose
Currently, this playbook is only intened to be executed against a Ubuntu server.  Maybe in the future I'll add Red Hat support in my AWS environment.

### Sudo Permissions
The account you use for Ansible to connect to the target server must have sudo permissions to be able to sudo to root and the chosen foundry user accounts.

### Foundry VTT
You have to acquire your own Foundry VTT zip file and put it in the ```files``` directory.  Update the playbooks to reference whatever your Foundry zip file name is.  I will make this a variable in the next iteration so you can update it in host_vars or group_vars insatead of the plabyooks.  I just didn't get that far yet.

### Proxy Files
Everyone's proxy config file is going to be different.  This process doesn't account for setting a config file from scratch, though it does contain a template config file you can use to get you started.  The template config file I have in the repo assumes you are using CertBot/LetsEncrypt to manage your TLS web certificates.

## File Naming Convention
| Filename Syntax | Type | Description |
| --- | --- | --- |
| wf_* | Workflow | Use these to execute multiple Playbooks |
| pb_* | Playbook | Use these to execute multiple Tasks |
| task_* | Task | Use these to execute a small number of tasks for a simple goal |

# Some Notes About This Project
## Random Stuff
1. This is something I did for fun to learn how to multi-instance FoundryVTT and improve my Ansible skills.  Use this at your own risk.
1. If you need help with Ansible, the best place to reach me is on the Ansible forum @Dustin-Wi or @Dustin (both are me).
    - [Ansible Forum](https://forum.ansible.com)
1. If you want help with this project, open an issue in this GitLab project.

## PM2
There is no official pm2 ansible module.  Instead of relying on [ansible-modules-pm2](https://github.com/10sr/ansible-modules-pm2) creating by [10sr](https://github.com/10sr), I just used ```ansible.builtin.shell``` and ```ansible.builtin.command```.  Because of this, I used ```ignore_errors``` on those tasks because pm2 uses different exit codes even if the command completes successfully.

Don't panic if you see errors in your output.  If the errors are on these particular tasks, it may be intended behavior.

Maybe if I get ambitious, I'll make my own galaxy collection for managing pm2 services.  Not likely at this point.

# Inventory
This project is really only intended to use a single target server in its inventory.  I use a simple inventory file, but you can do whatever makes you happy with your inventory file.  Hypothetically, you could have multiple servers and ```foundries``` variables defined in the host_vars for each inventory item.  You would just need to make sure you remember which server is supposed to have which foundry instances.

# Host Vars
For the playbooks to run correct, these are the minimum required variables.
| VAR Name | Type | Description |
| --- | --- | --- |
| foundries | List of Dict | Used to identify all of the intended foundry installations.  Example:<br> ```foundries: [{ 'name': 'Foundry1', 'port': 30001 }, { 'name': 'Foundry2', 'port': 30002 }]```
| foundry_username | String | The username that will be created on the target server.  This user will be used to manage all of the foundry configuration.<br>This user does not need sudo permisions to operate correctly.<br>If this account does not exist, ansible will create it for you.|
| foundry_password | String | The password for the foundry account on the target server.

# Usage
After setting up your inventory file, simply execute the ```wf_full_deploy.yml``` file and wait for it to complete.
```bash
dustin@Dustin-PC$ ansible-playbook wf_full_deploy.yml

PLAY [DEPLOY SERVICES] *************************************************************************************************************************************************************************************************************************************************************

TASK [Gathering Facts] *************************************************************************************************************************************************************************************************************************************************************
ok: [foundry_server_1]

TASK [Stop Execution For Non Ubuntu Systems] ***************************************************************************************************************************************************************************************************************************************
skipping: [foundry_server_1]

TASK [Install NGINX] ***************************************************************************************************************************************************************************************************************************************************************
ok: [foundry_server_1]

TASK [Add NodeJS Apt Key] **********************************************************************************************************************************************************************************************************************************************************
ok: [foundry_server_1]

TASK [Add NodeJS Repository] *******************************************************************************************************************************************************************************************************************************************************
ok: [foundry_server_1]

TASK [Install NodeJS] **************************************************************************************************************************************************************************************************************************************************************
ok: [foundry_server_1]

TASK [Install pm2] *****************************************************************************************************************************************************************************************************************************************************************
ok: [foundry_server_1]

TASK [Create Foundry User] *********************************************************************************************************************************************************************************************************************************************************
ok: [foundry_server_1]

TASK [Generate pm2 startup command] ************************************************************************************************************************************************************************************************************************************************
fatal: [foundry_server_1]: FAILED! => {"changed": true, "cmd": ["pm2", "startup"], "delta": "0:00:00.429896", "end": "2024-03-03 00:35:40.786439", "msg": "non-zero return code", "rc": 1, "start": "2024-03-03 00:35:40.356543", "stderr": "", "stderr_lines": [], "stdout": "[PM2] Init System found: systemd\n[PM2] To setup the Startup Script, copy/paste the following command:\nsudo env PATH=$PATH:/usr/bin /usr/lib/node_modules/pm2/bin/pm2 startup systemd -u user --hp /home/user", "stdout_lines": ["[PM2] Init System found: systemd", "[PM2] To setup the Startup Script, copy/paste the following command:", "sudo env PATH=$PATH:/usr/bin /usr/lib/node_modules/pm2/bin/pm2 startup systemd -u user --hp /home/user"]}
...ignoring

TASK [Create pm2 startup links] ****************************************************************************************************************************************************************************************************************************************************
changed: [foundry_server_1]

TASK [Create Install Directories] **************************************************************************************************************************************************************************************************************************************************
ok: [foundry_server_1] => (item=testing1)
ok: [foundry_server_1] => (item=testing2)
changed: [foundry_server_1] => (item=testing3)

TASK [Create UserData Directories] *************************************************************************************************************************************************************************************************************************************************
ok: [foundry_server_1] => (item=testing1)
ok: [foundry_server_1] => (item=testing2)
changed: [foundry_server_1] => (item=testing3)

TASK [Copy Foundry Installation To Server] *****************************************************************************************************************************************************************************************************************************************
ok: [foundry_server_1]

TASK [Extract Foundry Install Files] ***********************************************************************************************************************************************************************************************************************************************
ok: [foundry_server_1] => (item=testing1)
ok: [foundry_server_1] => (item=testing2)
changed: [foundry_server_1] => (item=testing3)

TASK [GET PM2 List] ****************************************************************************************************************************************************************************************************************************************************************
ok: [foundry_server_1]

TASK [Cleanup PM2 Service List] ****************************************************************************************************************************************************************************************************************************************************
ok: [foundry_server_1]

TASK [Start PM2 Service If Not Already Running] ************************************************************************************************************************************************************************************************************************************
skipping: [foundry_server_1] => (item={'name': 'testing1', 'port': 30001})
skipping: [foundry_server_1] => (item={'name': 'testing2', 'port': 30002})
changed: [foundry_server_1] => (item={'name': 'testing3', 'port': 30003})

TASK [Save PM2 Services Config] ****************************************************************************************************************************************************************************************************************************************************
ok: [foundry_server_1]

TASK [Update Foundry Options] ******************************************************************************************************************************************************************************************************************************************************
ok: [foundry_server_1] => (item=testing1)
ok: [foundry_server_1] => (item=testing2)
changed: [foundry_server_1] => (item=testing3)

TASK [Restart Changed PM2 Services] ************************************************************************************************************************************************************************************************************************************************
skipping: [foundry_server_1] => (item=testing1)
skipping: [foundry_server_1] => (item=testing2)
changed: [foundry_server_1] => (item=testing3)

TASK [Deploy NGINX 02_foundry Config] **********************************************************************************************************************************************************************************************************************************************
changed: [foundry_server_1]

TASK [Restart NGINX] ***************************************************************************************************************************************************************************************************************************************************************
changed: [foundry_server_1]

PLAY [REMOVE UNCONFIGURED SERVICES] ************************************************************************************************************************************************************************************************************************************************

TASK [Gathering Facts] *************************************************************************************************************************************************************************************************************************************************************
ok: [foundry_server_1]

TASK [Stop Execution For Non Ubuntu Systems] ***************************************************************************************************************************************************************************************************************************************
skipping: [foundry_server_1]

TASK [Get List of PM2 Services] ****************************************************************************************************************************************************************************************************************************************************
ok: [foundry_server_1]

TASK [Cleanup PM2 List] ************************************************************************************************************************************************************************************************************************************************************
ok: [foundry_server_1]

TASK [Extract Service Data From PM2 List] ******************************************************************************************************************************************************************************************************************************************
ok: [foundry_server_1]

TASK [set_fact] ********************************************************************************************************************************************************************************************************************************************************************
ok: [foundry_server_1]

TASK [Stop PM2 Service For Unwanted Services] **************************************************************************************************************************************************************************************************************************************
skipping: [foundry_server_1] => (item=testing2)
skipping: [foundry_server_1] => (item=testing1)
skipping: [foundry_server_1] => (item=testing3)
skipping: [foundry_server_1]

TASK [Delete Service's Foundry Directory] ******************************************************************************************************************************************************************************************************************************************
skipping: [foundry_server_1] => (item=testing2)
skipping: [foundry_server_1] => (item=testing1)
skipping: [foundry_server_1] => (item=testing3)
skipping: [foundry_server_1]

TASK [Deploy NGINX 02_foundry Config Changes] **************************************************************************************************************************************************************************************************************************************
ok: [foundry_server_1]

TASK [Restart NGINX If Changed] ****************************************************************************************************************************************************************************************************************************************************
skipping: [foundry_server_1]

PLAY RECAP *************************************************************************************************************************************************************************************************************************************************************************
foundry_server_1      : ok=27   changed=10   unreachable=0    failed=0    skipped=3    rescued=0    ignored=1

```