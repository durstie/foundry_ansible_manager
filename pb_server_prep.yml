---
- name: ============== PREPARE SERVER ==============
  hosts: foundry
  become: true
  become_user: root
  become_method: sudo
  become_flags: '-i'

  tasks:
    - name: Enable More Repos - RHEL Specific
      ansible.builtin.package:
        name:
          - epel-release
        state: present
      when: (ansible_distribution | lower) in ['centos']
    
    - name: Update All Packages - Ubuntu
      ansible.builtin.apt:
        name: "*"
        update_cache: yes
        state: latest
      when: (ansible_distribution | lower) in ['ubuntu']
    
    - name: Update All Packages - RHEL
      ansible.builtin.yum:
        name: "*"
        update_cache: yes
        state: latest
      when: (ansible_distribution | lower) in ['centos']

    - name: Install System Packages
      ansible.builtin.package:
        name:
          - certbot
          - python3-certbot-nginx
        state: present
      # when: (ansible_distribution | lower) in ['centos']

    - name: Add NodeJS Apt Key - Ubuntu Specific
      ansible.builtin.shell: "apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 2F59B5F99B1BE0B4"
      when: (ansible_distribution | lower) in ['ubuntu']

    - name: Add NodeJS Repository - Ubuntu Specific
      ansible.builtin.apt_repository:
        repo: deb https://deb.nodesource.com/node_20.x nodistro main
        update_cache: true
        state: present
      when: (ansible_distribution | lower) in ['ubuntu']

    - name: Install System Packages
      ansible.builtin.package:
        name:
          - nginx
          - nodejs
          - unzip
        state: present
    
    - name: Create Basic Foundry NGINX Config
      ansible.builtin.template:
        src: templates/02_foundry_basic_nginx.j2
        dest: "{{ nginx_site_dir }}/02_foundry.conf"
        force: no
      register: basic_config
    
    - name: Restart NGINX to Pick up Basic Config
      ansible.builtin.service:
        name: nginx
        state: restarted
      when: basic_config.changed
    
    - name: Get TLS Cert and Update NGINX Config with LETSENCRYPT/CertBot
      ansible.builtin.shell: "certbot run -n --nginx --agree-tos --keep -m {{ admin_email }} -d {{ inventory_hostname }}"

    - name: Install pm2
      community.general.npm:
        name: pm2
        global: true
        state: present

    - name: Create Foundry User
      ansible.builtin.user:
        name: "{{ foundry_username }}"
        shell: /bin/bash
        password: "{{ foundry_password | password_hash('sha512', salt=(foundry_username + foundry_password | regex_replace('[^0-9a-zA-Z]', ''))) }}"
        comment: Account to manage Foundry VTT Instances
    
    - name: Generate pm2 startup command
      become_user: "{{ foundry_username }}"
      ignore_errors: true
      ansible.builtin.command: "/usr/local/bin/pm2 startup"
      register: pm2_startup

    - name: Create pm2 startup links
      ansible.builtin.command: "{{ pm2_startup.stdout_lines | last }}"
      when: "'copy/paste the following command' in pm2_startup.stdout"
    
    # This one is not recommended in production.
    # Once I figure out how to get foundryvtt to work with SELinux, I'll change this
    - name: Disable SELinux
      ansible.posix.selinux:
        state: disabled
      register: selinux
      when: (ansible_distribution | lower) in ['centos']
    
    # To apply the SELinux change, you need to reboot the server
    - name: Reboot Server
      ansible.builtin.reboot:
        reboot_timeout: 300
      when: selinux is changed and (ansible_distribution | lower) in ['centos']
